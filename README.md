# learn-python

## SETUP THE ENVIRONMENT

```bash
sudo apt install python3-venv
python3 -m venv my_venv
which pip
source my_venv/bin/activate
which pip
which python
sudo apt install direnv
vim ~/.bashrc
echo 'eval "$(direnv hook bash)"' >> ~/.bashrc 
vim .envrc
echo source my_venv/bin/activate >> .envrc
direnv allow
```

## ORGANIZE THE PROJECT

```bash
klimeto@D1RI0363701:~/PycharmProjects/learn-python$ tree tests/
tests/
├── __init__.py
├── package
│   ├── __init__.py
│   ├── __pycache__
│   │   ├── __init__.cpython-37.pyc
│   │   └── submodule_test.cpython-37-pytest-5.1.3.pyc
│   └── submodule_test.py
└── __pycache__
    └── __init__.cpython-37.pyc

3 directories, 6 files
klimeto@D1RI0363701:~/PycharmProjects/learn-python$ tree package/
package/
├── __init__.py
├── __pycache__
│   ├── __init__.cpython-37.pyc
│   └── submodule.cpython-37.pyc
└── submodule.py

1 directory, 4 files


```
